﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class SuratJalanDetailViewModel
    {
        public int Id { get; set; }
        public int SuratJalanId { get; set; }
        public int BarangDetailId { get; set; }
        public string NamaDriver { get; set; }
        public string NomorPolisi { get; set; }
        public string NomorGudang { get; set; }
        public System.DateTime TglSuratJalan { get; set; }
        public int DriverId { get; set; }
        public int GudangId { get; set; }
        public int KendaraanId { get; set; }
        public string KodeSuratJalan { get; set; }

    }
}

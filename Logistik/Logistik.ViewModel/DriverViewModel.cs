﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class DriverViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Nama Driver")]
        public string Nama { get; set; }
        [Display(Name = "Alamat")]
        public string Alamat { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Display(Name ="Tipe")]
        public string Tipe { get; set; }
    }
}

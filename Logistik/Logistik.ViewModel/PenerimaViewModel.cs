﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PenerimaViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Nama Penerima")]
        public string NamaPenerima { get; set; }
        [Display(Name = "Alamat")]
        public string AlamatPenerima { get; set; }
        public int KotaTujuanId { get; set; }
        [Display(Name = "Kode Pos")]
        public string KodePos { get; set; }
        [Display (Name ="No Telpon")]
        public string NoTelp { get; set; }
        [Display (Name ="Kota Penerima")]
        public string NamaKota { get; set; }
        public decimal Harga { get; set; }
    }
}

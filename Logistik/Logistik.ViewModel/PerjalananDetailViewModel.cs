﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PerjalananDetailViewModel
    {
        public int Id { get; set; }
        public int PerjalananId { get; set; }
        public string KodePerjalanan { get; set; }
        public int TotalBeratPerjalanan { get; set; }
        public int DriverId { get; set; }
        public string NamaDriver { get; set; }
        public string AlamatDriver { get; set; }
        public string StatusDriver { get; set; }
        public string TipeDriver { get; set; }
        public int KendaraanId { get; set; }
        public string NoPolKendaraan { get; set; }
        public string MerkKendaraan { get; set; }
        public string StatusKendaraan { get; set; }
        public System.DateTime TglPerjalanan { get; set; }
        public int PengirimanId { get; set; }
        public string KodePengiriman { get; set; }
        public int JenisPengirimanId { get; set; }
        public string NamaJenisPengiriman { get; set; }
        public decimal HargaJenisPengiriman { get; set; }
        public System.DateTime TglPengiriman { get; set; }
        public decimal TotalHarga { get; set; }
        public int TotalBeratPengiriman { get; set; }

        public List<PengirimanDetailViewModel> ListPengirimanDetail { get; set; }
        public List<PengirimanViewModel> ListPengiriman { get; set; }
        public List<DriverViewModel> ListDriver { get; set; }
        public List<KendaraanViewModel> ListKendaraan { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class GudangViewModel
    {
        public int Id { get; set; }
        [Display(Name="Nomor Gudang")]
        public string NoGudang { get; set; }
        public string Alamat { get; set; }
    }
}

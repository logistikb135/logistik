﻿using Logistik.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PengirimanDetailViewModel
    {
        public int Id { get; set; }
        public int PengirimanId { get; set; }
        public string KodePengiriman { get; set; }
        public int JenisPengirimanId { get; set; }
        public string NamaJenisPengiriman { get; set; }
        public decimal HargaJenisPengiriman { get; set; }
        public System.DateTime TglPengiriman { get; set; }
        public decimal TotalHarga { get; set; }
        public int TotalBerat { get; set; }
        public int BarangDetailId { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public int Berat { get; set; }
        public string Keterangan { get; set; }
        public string NamaPengirim { get; set; }
        public string KodePosPengirim { get; set; }
        public string NoTelpPengirim { get; set; }
        public int KotaAsalId { get; set; }
        public int KotaTujuanId { get; set; }
        public string KotaAsal { get; set; }
        public decimal HargaKotaAsal { get; set; }
        public string NamaPenerima { get; set; }
        public string AlamatPenerima { get; set; }
        public string KodePosPenerima { get; set; }
        public string NoTelpPenerima { get; set; }
        public string KotaTujuan { get; set; }
        public decimal HargaKotaTujuan { get; set; }
        public string NoGudang { get; set; }
        public string Alamat { get; set; }
        public int Qty { get; set; }
        public decimal SubTotal { get; set; }

        public BarangDetailViewModel[] BarangDetail { get; set; }
    }
}

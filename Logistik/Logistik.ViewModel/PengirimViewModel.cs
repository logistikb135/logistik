﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PengirimViewModel
    {
        public int Id { get; set; }
        public string NamaPengirim { get; set; }
        public int KotaAsalId { get; set; }
        public string KodePos { get; set; }
        public string NoTelp { get; set; }
        public string NamaKota { get; set; }
        public decimal Harga { get; set; }
    }
}

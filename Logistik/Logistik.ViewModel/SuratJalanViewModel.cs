﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class SuratJalanViewModel
    {
        public int Id { get; set; }
        public string KodeSuratJalan { get; set; }
        public System.DateTime TglSuratJalan { get; set; }
        public int DriverId { get; set; }
        public int KendaraanId { get; set; }
        public int GudangId { get; set; }
        public string NamaDriver { get; set; }
        public string MerkKendaraan { get; set; }
        public string NomorGudang { get; set; }
        public string NomorPolisi { get; set; }
        public BarangDetailViewModel[] BarangDetail { get; set; }
        public List<DriverViewModel> ListDriver { get; set; }
        public List<KendaraanViewModel> ListKendaraan { get; set; }
        public List<GudangViewModel> ListGudang { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class ReturViewModel
    {
        public int Id { get; set; }
        public System.DateTime TglRetur { get; set; }
        public int PerjalananId { get; set; }
        public List<PerjalananViewModel> ListPerjalanan { get; set; }
    }
}

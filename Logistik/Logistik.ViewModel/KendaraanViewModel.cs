﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class KendaraanViewModel
    {
        public int Id { get; set; }
        [Display (Name ="Nomor Polisi")]
        public string NoPol { get; set; }
        [Display (Name ="Merek Kendaraan")]
        public string Merk { get; set; }
        [Display (Name ="Status")]
        public string Status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class JenisPengirimanViewModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public decimal Harga { get; set; }
    }
}

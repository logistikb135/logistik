﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PerjalananViewModel
    {
        public int Id { get; set; }
        public string KodePerjalanan { get; set; }
        public int TotalBerat { get; set; }
        public int DriverId { get; set; }
        public string NamaDriver { get; set; }
        public string AlamatDriver { get; set; }
        public string StatusDriver { get; set; }
        public string TipeDriver { get; set; }
        public int KendaraanId { get; set; }
        public string NoPolKendaraan { get; set; }
        public string MerkKendaraan { get; set; }
        public string StatusKendaraan { get; set; }
        public System.DateTime TglPerjalanan { get; set; }
    }
}

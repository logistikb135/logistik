﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class BarangViewModel
    {
        public int Id { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public int Berat { get; set; }
        public string Keterangan { get; set; }
        public string KodePengiriman { get; set; }
    }
}

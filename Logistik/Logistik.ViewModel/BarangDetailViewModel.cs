﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class BarangDetailViewModel
    {
        public int Id { get; set; }
        public int BarangId { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public int Berat { get; set; }
        public string Keterangan { get; set; }
        public int PengirimId { get; set; }
        public string NamaPengirim { get; set; }
        public string KodePosPengirim { get; set; }
        public string NoTelpPengirim { get; set; }
        public string KotaAsal { get; set; }
        public decimal HargaKotaAsal { get; set; }
        public int PenerimaId { get; set; }
        public string NamaPenerima { get; set; }
        public string AlamatPenerima { get; set; }
        public string KodePosPenerima { get; set; }
        public string NoTelpPenerima { get; set; }
        public string KotaTujuan { get; set; }
        public decimal HargaKotaTujuan { get; set; }
        public int Qty { get; set; }
        public decimal SubTotal { get; set; }
        public int PengirimanDetailId { get; set; }
        public string KodePengiriman { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class KotaViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Nama Kota")]
        public string NamaKota { get; set; }
        public decimal Harga { get; set; }
    }
}

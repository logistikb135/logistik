﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.ViewModel
{
    public class PengirimanViewModel
    {
        public int Id { get; set; }
        public string KodePengiriman { get; set; }
        public int JenisPengirimanId { get; set; }
        public string NamaJenisPengiriman { get; set; }
        public decimal HargaJenisPengiriman { get; set; }
        public System.DateTime TglPengiriman { get; set; }
        public decimal TotalHarga { get; set; }
        public int TotalBerat { get; set; }
    }
}

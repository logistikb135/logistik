﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class GudangDAL
    {
        public static List<GudangViewModel> Get()
        {
            List<GudangViewModel> model = new List<GudangViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.Gudang.Select(x => new GudangViewModel()
                {
                    Id = x.Id,
                    NoGudang = x.NoGudang,
                    Alamat = x.Alamat
                }).ToList();
            }
            return model;
        }
        public static GudangViewModel Get(int id)
        {
            GudangViewModel model = new GudangViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.Gudang.Where(x => x.Id == id).Select(x => new GudangViewModel()
                {
                    Id = x.Id,
                    NoGudang = x.NoGudang,
                    Alamat = x.Alamat
                }).FirstOrDefault();
            }
            return model;
        }
        public static bool InsertData(GudangViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Gudang.Add(new Gudang()
                {
                    Id = model.Id,
                    NoGudang = model.NoGudang,
                    Alamat = model.Alamat
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }
            return result;
        }
        public static bool UpdateData(GudangViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Gudang item = db.Gudang.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    item.NoGudang = model.NoGudang;
                    item.Alamat = model.Alamat;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
        public static bool DeleteData(GudangViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Gudang item = db.Gudang.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    db.Gudang.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

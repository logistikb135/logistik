﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class DriverDAL
    {
        public static List<DriverViewModel> Get()
        {
            List<DriverViewModel> model = new List<DriverViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = db.Driver.Select(x => new DriverViewModel()
                {
                    Id=x.Id,
                    Nama=x.Nama,
                    Alamat=x.Alamat,
                    Status=x.Status,
                    Tipe=x.Tipe
                }).ToList();
            }
            return model;
        }
        public static DriverViewModel Get(int id)
        {
            DriverViewModel model = new DriverViewModel();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = db.Driver.Where(x => x.Id == id).Select(x => new DriverViewModel()
                {
                    Id=x.Id,
                    Nama=x.Nama,
                    Alamat=x.Alamat,
                    Status=x.Status,
                    Tipe=x.Tipe
                }).FirstOrDefault();
            }
            return model;
        }
        public static bool InsertData(DriverViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                db.Driver.Add(new Driver()
                {
                    //Id=model.Id,
                    Nama=model.Nama,
                    Alamat=model.Alamat,
                    Status=model.Status,
                    Tipe=model.Tipe
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }
            return result;
        }
        public static bool EditData(DriverViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Driver item = db.Driver.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    item.Nama = model.Nama;
                    item.Alamat = model.Alamat;
                    item.Tipe = model.Tipe;
                    item.Status = model.Status;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
        public static bool DeleteData(DriverViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Driver item = db.Driver.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    db.Driver.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

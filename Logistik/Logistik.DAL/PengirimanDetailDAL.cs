﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class PengirimanDetailDAL
    {
        public static List<PengirimanDetailViewModel> Get(int id)
        {
            List<PengirimanDetailViewModel> list = new List<PengirimanDetailViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = db.PengirimanDetail.Where(x => x.PengirimanId == id).Select(x => new PengirimanDetailViewModel()
                {
                    Id = x.Id,
                    PengirimanId = x.PengirimanId,
                    Qty = x.Qty,
                    SubTotal = x.SubTotal
                }).ToList();
            }
            return list;
        }
    }
}

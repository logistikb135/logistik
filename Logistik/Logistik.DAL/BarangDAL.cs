﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class BarangDAL
    {
        public static List<BarangDetailViewModel> Get()
        {
            List<BarangDetailViewModel> list = new List<BarangDetailViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                list = (from a in db.Barang
                 join b in db.BarangDetail on a.Id equals b.BarangId
                 join c in db.PengirimanDetail on b.PengirimanDetailId equals c.Id
                 join d in db.Pengiriman on c.PengirimanId equals d.Id
                 select new BarangDetailViewModel()
                 {
                     Id = b.Id,
                     Berat = a.Berat,
                     Keterangan = a.Keterangan,
                     NamaBarang = a.NamaBarang,
                     Satuan = a.Satuan,
                     KodePengiriman=d.KodePengiriman
                 }).ToList();
            }
            return list;


        }
        public static BarangViewModel Get(int id)
        {
            BarangViewModel model = new BarangViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.Barang.Where(x => x.Id == id).Select(x => new BarangViewModel()
                {
                    Id = x.Id,
                    Berat = x.Berat,
                    Keterangan = x.Keterangan,
                    NamaBarang = x.NamaBarang,
                    Satuan = x.Satuan
                }).FirstOrDefault();
            }
            return model;
        }
    }
}

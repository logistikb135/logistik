﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class JenisPengirimanDAL
    {
        public static List<JenisPengirimanViewModel> Get()
        {
            List<JenisPengirimanViewModel> list = new List<JenisPengirimanViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = db.JenisPengiriman.Select(x => new JenisPengirimanViewModel()
                {
                    Id = x.Id,
                    Harga = x.Harga,
                    Nama = x.Nama
                }).ToList();
            }
            return list;
        }

        public static JenisPengirimanViewModel Get(int id)
        {
            JenisPengirimanViewModel model = new JenisPengirimanViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.JenisPengiriman.Where(x => x.Id == id).Select(x => new JenisPengirimanViewModel()
                {
                    Id = x.Id,
                    Harga = x.Harga,
                    Nama = x.Nama
                }).FirstOrDefault();
            }
            return model;
        }

        public static bool Insert(JenisPengirimanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.JenisPengiriman.Add(new JenisPengiriman()
                {
                    Nama = model.Nama,
                    Harga = model.Harga
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Edit(JenisPengirimanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                JenisPengiriman item = db.JenisPengiriman.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    item.Nama = model.Nama;
                    item.Harga = model.Harga;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static bool Delete(JenisPengirimanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                JenisPengiriman item = db.JenisPengiriman.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    db.JenisPengiriman.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

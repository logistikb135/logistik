﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class PenerimaDAL
    {
        public static List<PenerimaViewModel> Get()
        {
            List<PenerimaViewModel> list = new List<PenerimaViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                list = (from p in db.Penerima join k in db.Kota 
                        on p.KotaTujuanId equals k.Id
                        select new PenerimaViewModel()
                        {
                            Id=p.Id,
                            NamaPenerima=p.NamaPenerima,
                            AlamatPenerima=p.AlamatPenerima,
                            KodePos=p.KodePos,
                            KotaTujuanId=p.KotaTujuanId,
                            NoTelp=p.NoTelp,
                            Harga=k.Harga,
                            NamaKota=k.NamaKota
                        }).ToList();
            }
            return list;
        }

        public static PenerimaViewModel Get(int id)
        {
            PenerimaViewModel model = new PenerimaViewModel();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = (from p in db.Penerima
                         join k in db.Kota 
                         on p.KotaTujuanId equals k.Id
                         where p.Id == id
                         select new PenerimaViewModel()
                         {
                             Id = p.Id,
                             NamaPenerima = p.NamaPenerima,
                             AlamatPenerima = p.AlamatPenerima,
                             KodePos = p.KodePos,
                             KotaTujuanId = p.KotaTujuanId,
                             NoTelp = p.NoTelp,
                             Harga = k.Harga,
                             NamaKota = k.NamaKota
                         }).FirstOrDefault();
            }
            return model;
        }
        public static bool InsertData(PenerimaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Kota.Add(new Kota
                {
                    Harga = model.Harga,
                    NamaKota = model.NamaKota
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                Kota[] item = db.Kota.ToArray();
                Kota lastitem = item[item.Length - 1];
                db.Penerima.Add(new Penerima()
                {
                    NamaPenerima=model.NamaPenerima,
                    AlamatPenerima=model.AlamatPenerima,
                    KodePos=model.KodePos,
                    KotaTujuanId=lastitem.Id,
                    NoTelp=model.NoTelp
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }
        
            return result;
        }

        public static bool InsertExistingKota(PenerimaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                db.Penerima.Add(new Penerima()
                {
                    NamaPenerima=model.NamaPenerima,
                    AlamatPenerima=model.AlamatPenerima,
                    KotaTujuanId=model.KotaTujuanId,
                    KodePos=model.KodePos,
                    NoTelp=model.NoTelp
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }
            return result;
        }

        public static bool EditData(PenerimaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Penerima item = db.Penerima.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    item.NamaPenerima = model.NamaPenerima;
                    item.AlamatPenerima = model.AlamatPenerima;
                    item.KotaTujuanId = model.KotaTujuanId;
                    item.KodePos = model.KodePos;
                    item.NoTelp = model.NoTelp;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static bool DeleteData(PenerimaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Penerima item = db.Penerima.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    db.Penerima.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class BarangDetailDAL
    {
        public static List<BarangDetailViewModel> Get(List<PengirimanDetailViewModel> model)
        {
            List<BarangDetailViewModel> list = new List<BarangDetailViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                foreach (var item in model)
                {
                    list.Add((from a in db.BarangDetail
                              join b in db.PengirimanDetail on a.PengirimanDetailId equals b.Id
                              join c in db.Barang on a.BarangId equals c.Id
                              where a.PengirimanDetailId == item.Id
                              select new BarangDetailViewModel()
                              {
                                  NamaBarang = c.NamaBarang,
                                  Satuan = c.Satuan,
                                  Berat = c.Berat,
                                  Keterangan = c.Keterangan,
                                  Qty = b.Qty,
                                  SubTotal = b.SubTotal
                              }).FirstOrDefault());
                }
            }
            return list;
        }
        public static BarangDetailViewModel Get(int id)
        {
            BarangDetailViewModel model = new BarangDetailViewModel();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = (from a in db.BarangDetail
                         join b in db.Barang on a.BarangId equals b.Id
                         join c in db.PengirimanDetail on a.PengirimanDetailId equals c.Id
                         where a.Id == id
                         select new BarangDetailViewModel()
                         {
                            Id=a.Id,
                            NamaBarang=b.NamaBarang,
                            Satuan=b.Satuan,
                            Berat=b.Berat,
                            Keterangan=b.Keterangan
                         }).FirstOrDefault();
            }
            return model;
        }
    }
}

﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class PengirimDAL
    {
        public static List<PengirimViewModel> Get()
        {
            List<PengirimViewModel> list = new List<PengirimViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = (from p in db.Pengirim
                        join k in db.Kota on p.KotaAsalId equals k.Id
                        select new PengirimViewModel()
                        {
                            Id = p.Id,
                            NamaPengirim = p.NamaPengirim,
                            NoTelp = p.NoTelp,
                            KodePos = p.KodePos,
                            KotaAsalId = p.KotaAsalId,
                            NamaKota = k.NamaKota,
                            Harga = k.Harga
                        }).ToList();
            }
            return list;
        }

        public static PengirimViewModel Get(int id)
        {
            PengirimViewModel model = new PengirimViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = (from p in db.Pengirim
                         join k in db.Kota on p.KotaAsalId equals k.Id
                         where p.Id == id
                         select new PengirimViewModel()
                         {
                             Id = p.Id,
                             NamaPengirim = p.NamaPengirim,
                             NoTelp = p.NoTelp,
                             KodePos = p.KodePos,
                             KotaAsalId = p.KotaAsalId,
                             NamaKota = k.NamaKota,
                             Harga = k.Harga
                         }).FirstOrDefault();
            }
            return model;
        }

        public static bool Add(PengirimViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Kota.Add(new Kota()
                {
                    NamaKota = model.NamaKota,
                    Harga = model.Harga
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                Kota[] item = db.Kota.ToArray();
                Kota lastItem = item[item.Length - 1];
                db.Pengirim.Add(new Pengirim()
                {
                    NamaPengirim = model.NamaPengirim,
                    KodePos = model.KodePos,
                    NoTelp = model.NoTelp,
                    KotaAsalId = lastItem.Id
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool AddExistingKota(PengirimViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Pengirim.Add(new Pengirim()
                {
                    NamaPengirim = model.NamaPengirim,
                    KotaAsalId = model.KotaAsalId,
                    KodePos = model.KodePos,
                    NoTelp = model.NoTelp
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Edit(PengirimViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Pengirim item = db.Pengirim.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    item.NamaPengirim = model.NamaPengirim;
                    item.NoTelp = model.NoTelp;
                    item.KodePos = model.KodePos;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static bool Delete(PengirimViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Pengirim item = db.Pengirim.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    db.Pengirim.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class ReturDAL
    {
        public static List<ReturViewModel> Get()
        {
            List<ReturViewModel> list = new List<ReturViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                list = (from a in db.Retur
                        join b in db.Perjalanan on a.PerjalananId equals b.Id
                        select new ReturViewModel()
                        {
                            Id=a.Id,
                            TglRetur=a.TglRetur,
                            PerjalananId=a.PerjalananId
                        }).ToList();
            }
            return list;
        }
    }
}

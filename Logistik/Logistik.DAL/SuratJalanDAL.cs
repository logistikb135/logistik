﻿using Logistik.ViewModel;
using Logistik.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class SuratJalanDAL
    {
        public static List<SuratJalanViewModel> Get()
        {
            List<SuratJalanViewModel> list = new List<SuratJalanViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                list = (from a in db.SuratJalan
                        join b in db.Driver on a.DriverId equals b.Id
                        join c in db.Kendaraan on a.KendaraanId equals c.Id
                        join d in db.Gudang on a.GudangId equals d.Id
                        select new SuratJalanViewModel()
                        {
                            Id = a.Id,
                            KodeSuratJalan = a.KodeSuratJalan,
                            TglSuratJalan = a.TglSuratJalan,
                            NamaDriver=b.Nama,
                            NomorPolisi=c.NoPol,
                            MerkKendaraan=c.Merk,
                            NomorGudang=d.NoGudang
                        }).ToList();
            }
            return list;
        }
        public static SuratJalanViewModel Detail(int id)
        {
            SuratJalanViewModel model = new SuratJalanViewModel();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = (from a in db.SuratJalanDetail
                         join b in db.SuratJalan on a.SuratJalanId equals b.Id
                         join c in db.BarangDetail on a.BarangDetailId equals c.Id
                         join d in db.Kendaraan on b.KendaraanId equals d.Id
                         join e in db.Gudang on b.GudangId equals e.Id
                         join f in db.Driver on b.DriverId equals f.Id
                         where id == b.Id
                         select new SuratJalanViewModel()
                         {
                             Id=a.Id,
                             KodeSuratJalan=b.KodeSuratJalan,
                             NamaDriver=f.Nama,
                             MerkKendaraan=d.Merk,
                             NomorPolisi=d.NoPol,
                             NomorGudang=e.NoGudang,
                         }).FirstOrDefault();
            }
            return model;
        }
        public static bool Insert(SuratJalanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                db.SuratJalan.Add(new SuratJalan()
                {
                     KodeSuratJalan=model.KodeSuratJalan,
                     TglSuratJalan=DateTime.Now,
                     DriverId=model.DriverId,
                     KendaraanId=model.KendaraanId,
                     GudangId=model.GudangId
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                SuratJalan[] item = db.SuratJalan.ToArray();
                SuratJalan lastItem = item[item.Length - 1];
                for (int i = 0; i < model.BarangDetail.Length; i++)
                {
                    db.SuratJalanDetail.Add(new SuratJalanDetail()
                    {
                        BarangDetailId=model.BarangDetail[i].Id,
                        SuratJalanId=lastItem.Id
                    });
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

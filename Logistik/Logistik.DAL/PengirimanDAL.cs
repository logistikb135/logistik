﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class PengirimanDAL
    {
        public static List<PengirimanViewModel> Get()
        {
            List<PengirimanViewModel> list = new List<PengirimanViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = (from a in db.Pengiriman
                        join b in db.JenisPengiriman on a.JenisPengirimanId equals b.Id
                        select new PengirimanViewModel()
                        {
                            Id = a.Id,
                            KodePengiriman = a.KodePengiriman,
                            JenisPengirimanId = a.JenisPengirimanId,
                            NamaJenisPengiriman = b.Nama,
                            HargaJenisPengiriman = b.Harga,
                            TglPengiriman = a.TglPengiriman,
                            TotalBerat = a.TotalBerat,
                            TotalHarga = a.TotalHarga
                        }).ToList();
            }
            return list;
        }

        public static List<PengirimanDetailViewModel> Detail(int id)
        {
            List<PengirimanDetailViewModel> list = new List<PengirimanDetailViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = (from a in db.Pengiriman
                        join b in db.JenisPengiriman on a.JenisPengirimanId equals b.Id
                        join c in db.PerjalananDetail on a.Id equals c.PengirimanId
                        join d in db.PengirimanDetail on a.Id equals d.PengirimanId
                        join e in db.BarangDetail on d.Id equals e.PengirimanDetailId
                        join f in db.Penerima on e.PenerimaId equals f.Id
                        join g in db.Pengirim on e.PengirimId equals g.Id
                        join h in db.Kota on f.KotaTujuanId equals h.Id
                        join i in db.Kota on g.KotaAsalId equals i.Id
                        where c.PerjalananId == id
                        select new PengirimanDetailViewModel()
                        {
                            Id = a.Id,
                            KodePengiriman = a.KodePengiriman,
                            JenisPengirimanId = a.JenisPengirimanId,
                            NamaJenisPengiriman = b.Nama,
                            HargaJenisPengiriman = b.Harga,
                            TglPengiriman = a.TglPengiriman,
                            TotalBerat = a.TotalBerat,
                            TotalHarga = a.TotalHarga,
                            NamaPenerima = f.NamaPenerima,
                            NamaPengirim = g.NamaPengirim,
                            KotaAsal = i.NamaKota,
                            KotaTujuan = h.NamaKota,
                            AlamatPenerima = f.AlamatPenerima,
                            KodePosPenerima = f.KodePos,
                            KodePosPengirim = g.KodePos,
                            NoTelpPenerima = f.NoTelp,
                            NoTelpPengirim = g.NoTelp
                        }).ToList();
            }
            return list;
        }

        public static PengirimanDetailViewModel Get(int id)
        {
            PengirimanDetailViewModel model = new PengirimanDetailViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = (from a in db.Pengiriman
                         join b in db.JenisPengiriman on a.JenisPengirimanId equals b.Id
                         join c in db.PengirimanDetail on a.Id equals c.PengirimanId
                         join d in db.BarangDetail on c.Id equals d.PengirimanDetailId
                         join e in db.Barang on d.BarangId equals e.Id
                         join f in db.Pengirim on d.PengirimId equals f.Id
                         join g in db.Penerima on d.PenerimaId equals g.Id
                         join h in db.Kota on f.KotaAsalId equals h.Id
                         join i in db.Kota on g.KotaTujuanId equals i.Id
                         where a.Id == id
                         select new PengirimanDetailViewModel()
                         {
                             Id = c.Id,
                             PengirimanId = c.PengirimanId,
                             KodePengiriman = a.KodePengiriman,
                             NamaJenisPengiriman = b.Nama,
                             HargaJenisPengiriman = b.Harga,
                             TglPengiriman = a.TglPengiriman,
                             TotalHarga = a.TotalHarga,
                             TotalBerat = a.TotalBerat,
                             NamaPengirim = f.NamaPengirim,
                             KodePosPengirim = f.KodePos,
                             NoTelpPengirim = f.NoTelp,
                             KotaAsal = h.NamaKota,
                             NamaPenerima = g.NamaPenerima,
                             AlamatPenerima = g.AlamatPenerima,
                             KodePosPenerima = g.KodePos,
                             NoTelpPenerima = g.NoTelp,
                             KotaTujuan = i.NamaKota
                         }).FirstOrDefault();
            }
            return model;
        }

        public static bool Insert(PengirimanDetailViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Pengirim.Add(new Pengirim()
                {
                    NamaPengirim = model.NamaPengirim,
                    KotaAsalId = model.KotaAsalId,
                    KodePos = model.KodePosPengirim,
                    NoTelp = model.NoTelpPengirim
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                db.Penerima.Add(new Penerima()
                {
                    NamaPenerima = model.NamaPenerima,
                    AlamatPenerima = model.AlamatPenerima,
                    KotaTujuanId = model.KotaTujuanId,
                    KodePos = model.KodePosPenerima,
                    NoTelp = model.NoTelpPenerima
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                db.Pengiriman.Add(new Pengiriman()
                {
                    KodePengiriman = model.KodePengiriman,
                    JenisPengirimanId = model.JenisPengirimanId,
                    TglPengiriman = DateTime.Now,
                    TotalHarga = model.TotalHarga,
                    TotalBerat = model.TotalBerat
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                for (int i = 0; i < model.BarangDetail.Length; i++)
                {
                    db.Barang.Add(new Barang()
                    {
                        NamaBarang = model.BarangDetail[i].NamaBarang,
                        Satuan = model.BarangDetail[i].Satuan,
                        Berat = model.BarangDetail[i].Berat,
                        Keterangan = model.BarangDetail[i].Keterangan
                    });
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
                Pengiriman[] item = db.Pengiriman.ToArray();
                Pengiriman lastItem = item[item.Length - 1];
                for (int i = 0; i < model.BarangDetail.Length; i++)
                {
                    db.PengirimanDetail.Add(new PengirimanDetail()
                    {
                        PengirimanId = lastItem.Id,
                        Qty = model.BarangDetail[i].Qty,
                        SubTotal = model.BarangDetail[i].SubTotal
                    });
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
                Barang[] itemBarang = db.Barang.ToArray();
                Pengirim[] itemPengirim = db.Pengirim.ToArray();
                Pengirim lastItemPengirim = itemPengirim[itemPengirim.Length - 1];
                Penerima[] itemPenerima = db.Penerima.ToArray();
                Penerima lastItemPenerima = itemPenerima[itemPenerima.Length - 1];
                PengirimanDetail[] itemPengirimanDetail = db.PengirimanDetail.ToArray();
                int startIndex = itemBarang.Length - model.BarangDetail.Length;
                int j = itemPengirimanDetail.Length - model.BarangDetail.Length;
                for (int i = startIndex; i < itemBarang.Length; i++)
                {
                    db.BarangDetail.Add(new BarangDetail()
                    {
                        BarangId = itemBarang[i].Id,
                        PengirimId = lastItemPengirim.Id,
                        PenerimaId = lastItemPenerima.Id,
                        PengirimanDetailId = itemPengirimanDetail[j].Id
                    });
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                    j++;
                }
            }
            return result;
        }
    }
}

﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class PerjalananDAL
    {
        public static List<PerjalananViewModel> Get()
        {
            List<PerjalananViewModel> list = new List<PerjalananViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = (from a in db.Perjalanan
                        join b in db.Driver on a.DriverId equals b.Id
                        join c in db.Kendaraan on a.KendaraanId equals c.Id
                        select new PerjalananViewModel()
                        {
                            Id = a.Id,
                            KodePerjalanan = a.KodePerjalanan,
                            TglPerjalanan = a.TglPerjalanan,
                            TotalBerat = a.TotalBerat,
                            DriverId = b.Id,
                            NamaDriver = b.Nama,
                            AlamatDriver = b.Alamat,
                            StatusDriver = b.Status,
                            TipeDriver = b.Tipe,
                            KendaraanId = c.Id,
                            NoPolKendaraan = c.NoPol,
                            MerkKendaraan = c.Merk,
                            StatusKendaraan = c.Status
                        }).ToList();
            }
            return list;
        }

        public static List<PerjalananDetailViewModel> Get(int id)
        {
            List<PerjalananDetailViewModel> list = new List<PerjalananDetailViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                list = (from a in db.Perjalanan
                        join b in db.Driver on a.DriverId equals b.Id
                        join c in db.Kendaraan on a.KendaraanId equals c.Id
                        join d in db.PerjalananDetail on a.Id equals d.PerjalananId
                        join e in db.Pengiriman on d.PengirimanId equals e.Id
                        join f in db.JenisPengiriman on e.JenisPengirimanId equals f.Id
                        where a.Id == id
                        select new PerjalananDetailViewModel()
                        {
                            Id = d.Id,
                            PerjalananId = a.Id,
                            KodePerjalanan = a.KodePerjalanan,
                            TotalBeratPerjalanan = a.TotalBerat,
                            TglPerjalanan = a.TglPerjalanan,
                            DriverId = a.DriverId,
                            NamaDriver = b.Nama,
                            AlamatDriver = b.Alamat,
                            StatusDriver = b.Status,
                            TipeDriver = b.Tipe,
                            KendaraanId = a.KendaraanId,
                            NoPolKendaraan = c.NoPol,
                            MerkKendaraan = c.Merk,
                            StatusKendaraan = c.Status,
                            PengirimanId = d.PengirimanId,
                            KodePengiriman = e.KodePengiriman,
                            TglPengiriman = e.TglPengiriman,
                            TotalHarga = e.TotalHarga,
                            TotalBeratPengiriman = e.TotalBerat,
                            JenisPengirimanId = e.JenisPengirimanId,
                            NamaJenisPengiriman = f.Nama,
                            HargaJenisPengiriman = f.Harga
                        }).ToList();
            }
            return list;
        }

        public static PerjalananDetailViewModel Detail(int id)
        {
            PerjalananDetailViewModel model = new PerjalananDetailViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = (from a in db.PerjalananDetail
                         join b in db.Perjalanan on a.PerjalananId equals b.Id
                         join c in db.Driver on b.DriverId equals c.Id
                         join d in db.Kendaraan on b.KendaraanId equals d.Id
                         join e in db.Pengiriman on a.PengirimanId equals e.Id
                         join f in db.JenisPengiriman on e.JenisPengirimanId equals f.Id
                         where b.Id == id
                         select new PerjalananDetailViewModel()
                         {
                             Id = a.Id,
                             PerjalananId = b.Id,
                             KodePerjalanan = b.KodePerjalanan,
                             TotalBeratPerjalanan = b.TotalBerat,
                             DriverId = c.Id,
                             AlamatDriver = c.Alamat,
                             NamaDriver = c.Nama,
                             StatusDriver = c.Status,
                             TipeDriver = c.Tipe,
                             KendaraanId = d.Id,
                             NoPolKendaraan = d.NoPol,
                             MerkKendaraan = d.Merk,
                             StatusKendaraan = d.Status,
                             TglPerjalanan = b.TglPerjalanan,
                             PengirimanId = e.Id,
                             KodePengiriman = e.KodePengiriman,
                             JenisPengirimanId = f.Id,
                             NamaJenisPengiriman = f.Nama,
                             HargaJenisPengiriman = f.Harga,
                             TglPengiriman = e.TglPengiriman,
                             TotalHarga = e.TotalHarga,
                             TotalBeratPengiriman = e.TotalBerat
                         }).FirstOrDefault();
            }
            return model;
        }

        public static bool Insert(PerjalananDetailViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Perjalanan.Add(new Perjalanan()
                {
                    KodePerjalanan = model.KodePerjalanan,
                    TotalBerat = model.TotalBeratPerjalanan,
                    TglPerjalanan = DateTime.Now,
                    DriverId = model.DriverId,
                    KendaraanId = model.KendaraanId
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
                List<PerjalananViewModel> listPerjalanan = db.Perjalanan.Select(x => new PerjalananViewModel()
                {
                    Id = x.Id
                }).ToList();
                PerjalananViewModel perjalanan = listPerjalanan.LastOrDefault();
                foreach (var item in model.ListPengiriman)
                {
                    db.PerjalananDetail.Add(new PerjalananDetail()
                    {
                        PerjalananId = perjalanan.Id,
                        PengirimanId = item.Id
                    });
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

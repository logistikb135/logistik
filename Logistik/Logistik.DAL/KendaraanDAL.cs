﻿using System;
using Logistik.Model;
using Logistik.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class KendaraanDAL
    {
        public static List<KendaraanViewModel> Get()
        {
            List<KendaraanViewModel> model = new List<KendaraanViewModel>();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = db.Kendaraan.Select(x => new KendaraanViewModel()
                {
                    Id=x.Id,
                    NoPol=x.NoPol,
                    Merk=x.Merk,
                    Status=x.Status
                }).ToList();
            }
            return model;
        }
        public static KendaraanViewModel Get(int id)
        {
            KendaraanViewModel model = new KendaraanViewModel();
            using (LogistikEntities db=new LogistikEntities())
            {
                model = db.Kendaraan.Where(x => x.Id == id).Select(x => new KendaraanViewModel()
                {
                    Id=x.Id,
                    NoPol=x.NoPol,
                    Merk=x.Merk,
                    Status=x.Status
                }).FirstOrDefault();
            }
            return model;
        }
        public static bool InsertData(KendaraanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                db.Kendaraan.Add(new Kendaraan()
                {
                    Id = model.Id,
                    NoPol=model.NoPol,
                    Merk=model.Merk,
                    Status =model.Status
                });
                try
                {
                    db.SaveChanges();
                    result = true;

                }
                catch (Exception)
                {

                    result = false;
                }
            }
            return result;
        }
        public static bool UpdateData(KendaraanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Kendaraan item=db.Kendaraan.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    item.NoPol = model.NoPol;
                    item.Merk = model.Merk;
                    item.Status = model.Status;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {

                        result = false;
                    }
                }
            }
            return result;
        }
        public static bool DeleteData(KendaraanViewModel model)
        {
            bool result = false;
            using (LogistikEntities db=new LogistikEntities())
            {
                Kendaraan item = db.Kendaraan.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item!=null)
                {
                    db.Kendaraan.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

﻿using Logistik.Model;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistik.DAL
{
    public class KotaDAL
    {
        public static List<KotaViewModel> Get()
        {
            List<KotaViewModel> model = new List<KotaViewModel>();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.Kota.Select(x => new KotaViewModel()
                {
                    Id = x.Id,
                    Harga = x.Harga,
                    NamaKota = x.NamaKota
                }).ToList();
            }
            return model;
        }

        public static KotaViewModel Get(int id)
        {
            KotaViewModel model = new KotaViewModel();
            using (LogistikEntities db = new LogistikEntities())
            {
                model = db.Kota.Where(x => x.Id == id).Select(x => new KotaViewModel()
                {
                    Id = x.Id,
                    Harga = x.Harga,
                    NamaKota = x.NamaKota
                }).FirstOrDefault();
            }
            return model;
        }

        public static bool Insert(KotaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                db.Kota.Add(new Kota()
                {
                    Harga = model.Harga,
                    Id = model.Id,
                    NamaKota = model.NamaKota
                });
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Update(KotaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Kota kota = db.Kota.Where(x => x.Id == model.Id).FirstOrDefault();
                if (kota != null)
                {
                    kota.NamaKota = model.NamaKota;
                    kota.Harga = model.Harga;
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static bool Delete(KotaViewModel model)
        {
            bool result = false;
            using (LogistikEntities db = new LogistikEntities())
            {
                Kota item = db.Kota.Where(x => x.Id == model.Id).FirstOrDefault();
                if (item != null)
                {
                    db.Kota.Remove(item);
                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logistik.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class JenisPengiriman
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JenisPengiriman()
        {
            this.Pengiriman = new HashSet<Pengiriman>();
        }
    
        public int Id { get; set; }
        public string Nama { get; set; }
        public decimal Harga { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pengiriman> Pengiriman { get; set; }
    }
}

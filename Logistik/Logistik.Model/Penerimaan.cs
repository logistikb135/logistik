//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logistik.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Penerimaan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Penerimaan()
        {
            this.PenerimaanDetail = new HashSet<PenerimaanDetail>();
        }
    
        public int Id { get; set; }
        public System.DateTime TglPenerimaan { get; set; }
        public int PerjalananId { get; set; }
    
        public virtual Perjalanan Perjalanan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenerimaanDetail> PenerimaanDetail { get; set; }
    }
}

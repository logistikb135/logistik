﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class PengirimanController : Controller
    {
        // GET: Pengiriman
        public ActionResult Index()
        {
            List<PengirimanViewModel> list = PengirimanDAL.Get();
            return View(list);
        }

        public ActionResult Detail(int id)
        {
            PengirimanDetailViewModel model = PengirimanDAL.Get(id);
            List<PengirimanDetailViewModel> list = PengirimanDetailDAL.Get(id);
            BarangDetailViewModel[] barang = BarangDetailDAL.Get(list).ToArray();
            model.BarangDetail = barang;
            return View(model);
        }

        public ActionResult List()
        {
            List<PengirimanViewModel> list = PengirimanDAL.Get();
            return PartialView("_List", list);
        }

        public ActionResult ListKotaPengirim()
        {
            List<KotaViewModel> list = KotaDAL.Get();
            return PartialView("_ListKotaPengirim", list);
        }

        public ActionResult ListKotaPenerima()
        {
            List<KotaViewModel> list = KotaDAL.Get();
            return PartialView("_ListKotaPenerima", list);
        }

        public ActionResult DataKotaPengirim(int id)
        {
            KotaViewModel model = KotaDAL.Get(id);
            return PartialView("_DataKotaPengirim", model);
        }

        public ActionResult DataKotaPenerima(int id)
        {
            KotaViewModel model = KotaDAL.Get(id);
            return PartialView("_DataKotaPenerima", model);
        }

        public ActionResult TambahBarang()
        {
            return PartialView("_TambahBarang");
        }

        public ActionResult ListJenisPengiriman()
        {
            List<JenisPengirimanViewModel> list = JenisPengirimanDAL.Get();
            return PartialView("_ListJenisPengiriman", list);
        }

        public ActionResult DataJenisPengiriman(int id)
        {
            JenisPengirimanViewModel model = JenisPengirimanDAL.Get(id);
            return PartialView("_DataJenisPengiriman", model);
        }

        public ActionResult Insert()
        {
            PengirimanDetailViewModel model = new PengirimanDetailViewModel();
            BarangDetailViewModel[] barang = new List<BarangDetailViewModel>().ToArray();
            model.BarangDetail = barang;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(PengirimanDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (PengirimanDAL.Insert(model))
                {
                    return RedirectToAction("Index", "Pengiriman");
                }
            }
            return View(model);
        }
    }
}
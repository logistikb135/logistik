﻿using System;
using Logistik.DAL;
using Logistik.Model;
using Logistik.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class SuratJalanController : Controller
    {
        // GET: SuratJalan
        public ActionResult Index()
        {
            List<SuratJalanViewModel> list = SuratJalanDAL.Get();
            return View(list);
        }
        public ActionResult List()
        {
            List<SuratJalanViewModel> list = SuratJalanDAL.Get();
            return PartialView("_List",list);
        }
        public ActionResult Insert()
        {
            SuratJalanViewModel model = new SuratJalanViewModel();
            BarangDetailViewModel[] barang = new List<BarangDetailViewModel>().ToArray();
            List<KendaraanViewModel> list = KendaraanDAL.Get();
            List<GudangViewModel> list2 = GudangDAL.Get();
            List<DriverViewModel> list3 = DriverDAL.Get();
            model.BarangDetail = barang;
            model.ListDriver = list3;
            model.ListGudang = list2;
            model.ListKendaraan = list;
            return View(model);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Insert(SuratJalanViewModel model)
        {
            if (ModelState.IsValid&&SuratJalanDAL.Insert(model))
            {
                return RedirectToAction("Index", "SuratJalan");
            }
            return View(model);
        }
        public ActionResult ListDriver()
        {
            List<DriverViewModel> list = DriverDAL.Get();
            return PartialView("_ListDriver", list);
        }
        public ActionResult TambahBarang()
        {
            List<BarangDetailViewModel> list = BarangDAL.Get();
            return PartialView("_TambahBarang",list);
        }
        public ActionResult DataBarang(int id)
        {
            BarangViewModel model = BarangDAL.Get(id);
            return PartialView("_DataBarang", model);
        }
        public ActionResult ListBarang(int id)
        {
            BarangDetailViewModel model = BarangDetailDAL.Get(id);
            return PartialView("_ListBarang", model);
        }
        public ActionResult Detail(int id)
        {
            SuratJalanViewModel model = SuratJalanDAL.Detail(id);
            ////BarangDetailViewModel[] barang = 

            return View(model);
        }
    }
}
﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class KotaController : Controller
    {
        // GET: Kota
        public ActionResult Index()
        {
            return View(KotaDAL.Get());
        }

        public ActionResult Detail(int id)
        {
            return PartialView("_Detail", KotaDAL.Get(id));
        }

        public ActionResult Add()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(KotaViewModel model)
        {
            if (ModelState.IsValid && KotaDAL.Insert(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Add");
        }

        public ActionResult List()
        {
            return PartialView("_List", KotaDAL.Get());
        }

        public ActionResult Edit(int id)
        {
            return PartialView("_Edit", KotaDAL.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KotaViewModel model)
        {
            if (ModelState.IsValid && KotaDAL.Update(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }

        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", KotaDAL.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(KotaViewModel model)
        {
            if (ModelState.IsValid && KotaDAL.Delete(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }
    }
}
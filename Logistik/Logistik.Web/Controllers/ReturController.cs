﻿using Logistik.ViewModel;
using Logistik.Model;
using Logistik.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class ReturController : Controller
    {
        // GET: Retur
        public ActionResult Index()
        {
            List<ReturViewModel> list = ReturDAL.Get();
            return View(list);
        }
        public ActionResult Insert()
        {
            return View();
        }
    }
}
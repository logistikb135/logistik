﻿using System;
using Logistik.ViewModel;
using Logistik.DAL;
using Logistik.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class KendaraanController : Controller
    {
        // GET: Kendaraan
        public ActionResult Index()
        {
            List<KendaraanViewModel> list = KendaraanDAL.Get();
            return View(KendaraanDAL.Get());
        }

        public ActionResult List()
        {
            List<KendaraanViewModel> model = KendaraanDAL.Get();
            return PartialView("_List",model);
        }

        public ActionResult Detail(int id)
        {
            KendaraanViewModel model = KendaraanDAL.Get(id);
            return PartialView("_Detail", model);
        }

        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(KendaraanViewModel model)
        {
            if (ModelState.IsValid&&KendaraanDAL.InsertData(model))
            {
                var result=new {message="success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Add");
        }

        public ActionResult Edit(int id)
        {
            KendaraanViewModel model = KendaraanDAL.Get(id);
            return PartialView("_Edit",model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KendaraanViewModel model)
        {
            if (ModelState.IsValid&&KendaraanDAL.UpdateData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }

        public ActionResult Delete(int id)
        {
            KendaraanViewModel model = KendaraanDAL.Get(id);
            return PartialView("_Delete",model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(KendaraanViewModel model)
        {
            if (ModelState.IsValid&&KendaraanDAL.DeleteData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }
    }
}
﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class PenerimaController : Controller
    {
        // GET: Penerima
        public ActionResult Index()
        {
            List<PenerimaViewModel> list = PenerimaDAL.Get();
            return View(list);
        }
        public ActionResult List()
        {
            List<PenerimaViewModel> list = PenerimaDAL.Get();
            return PartialView("_List",list);
        }
        public ActionResult TambahKota()
        {
            return PartialView("_TambahKota");
        }
        public ActionResult PilihKota()
        {
            List<KotaViewModel> kota = KotaDAL.Get();
            return PartialView("_PilihKota", kota);
        }
        public ActionResult Detail(int id)
        {
            PenerimaViewModel penerima = PenerimaDAL.Get(id);
            return PartialView("_Detail", penerima);
        }
        public ActionResult Insert()
        {
            return PartialView("_Insert");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(PenerimaViewModel model)
        {
            if (ModelState.IsValid&&PenerimaDAL.InsertData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (ModelState.IsValid&&PenerimaDAL.InsertExistingKota(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Insert");
        }
        public ActionResult Edit(int id)
        {
            PenerimaViewModel penerima = PenerimaDAL.Get(id);
            return PartialView("_Edit", penerima);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PenerimaViewModel model)
        {
            if (ModelState.IsValid&&PenerimaDAL.EditData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }
        public ActionResult Delete(int id)
        {
            PenerimaViewModel penerima = PenerimaDAL.Get(id);
            return PartialView("_Delete", penerima);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(PenerimaViewModel model)
        {
            if (ModelState.IsValid&&PenerimaDAL.DeleteData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("Delete");
        }
    }
}
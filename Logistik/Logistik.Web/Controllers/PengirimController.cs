﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class PengirimController : Controller
    {
        // GET: Pengirim
        public ActionResult Index()
        {
            List<PengirimViewModel> list = PengirimDAL.Get();
            return View(list);
        }

        public ActionResult List()
        {
            List<PengirimViewModel> list = PengirimDAL.Get();
            return PartialView("_List", list);
        }

        public ActionResult TambahKota()
        {
            return PartialView("_TambahKota");
        }

        public ActionResult PilihKota()
        {
            List<KotaViewModel> kota = KotaDAL.Get();
            return PartialView("_PilihKota", kota);
        }

        public ActionResult Insert()
        {
            return PartialView("_Insert");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(PengirimViewModel model)
        {
            if (ModelState.IsValid && PengirimDAL.Add(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (ModelState.IsValid && PengirimDAL.AddExistingKota(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Insert");
        }

        public ActionResult Detail(int id)
        {
            PengirimViewModel pengirim = PengirimDAL.Get(id);
            return PartialView("_Detail", pengirim);
        }

        public ActionResult Edit(int id)
        {
            PengirimViewModel pengirim = PengirimDAL.Get(id);
            return PartialView("_Edit", pengirim);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PengirimViewModel model)
        {
            if (ModelState.IsValid && PengirimDAL.Edit(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }

        public ActionResult Delete(int id)
        {
            PengirimViewModel pengirim = PengirimDAL.Get(id);
            return PartialView("_Delete", pengirim);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(PengirimViewModel model)
        {
            if (ModelState.IsValid && PengirimDAL.Delete(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }
    }
}
﻿using Logistik.ViewModel;
using Logistik.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class GudangController : Controller
    {
        // GET: Gudang
        public ActionResult Index()
        {
            return View(GudangDAL.Get());
        }
        public ActionResult List()
        {
            List<GudangViewModel> model = GudangDAL.Get();
            return PartialView("_List", model);
        }
        public ActionResult Detail(int id)
        {
            GudangViewModel model = GudangDAL.Get(id);
            return PartialView("_Detail", model);
        }
        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(GudangViewModel model)
        {
            if (ModelState.IsValid && GudangDAL.InsertData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Add");
        }
        public ActionResult Edit(int id)
        {
            GudangViewModel model = GudangDAL.Get(id);
            return PartialView("_Edit", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GudangViewModel model)
        {
            if (ModelState.IsValid && GudangDAL.UpdateData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }
        public ActionResult Delete(int id)
        {
            GudangViewModel model = GudangDAL.Get(id);
            return PartialView("_Delete", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(GudangViewModel model)
        {
            if (ModelState.IsValid && GudangDAL.DeleteData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }

    }
}
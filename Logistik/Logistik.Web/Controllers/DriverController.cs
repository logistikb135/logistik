﻿using Logistik.Model;
using Logistik.ViewModel;
using Logistik.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class DriverController : Controller
    {
        // GET: Driver
        public ActionResult Index()
        {
            List<DriverViewModel> list = DriverDAL.Get();
            return View(list);
        }
        public ActionResult List()
        {
            List<DriverViewModel> model = DriverDAL.Get();
            return PartialView("_List", model);
        }
        public ActionResult Detail(int id)
        {
            DriverViewModel model = DriverDAL.Get(id);
            return PartialView("_Detail", model);
        }
        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(DriverViewModel model)
        {
            if (ModelState.IsValid&&DriverDAL.InsertData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Add");
        }
        public ActionResult Edit(int id)
        {
            DriverViewModel model = DriverDAL.Get(id);
            return PartialView("_Edit",model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DriverViewModel model)
        {
            if (ModelState.IsValid&&DriverDAL.EditData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }
        public ActionResult Delete(int id)
        {
            DriverViewModel model = DriverDAL.Get(id);
            return PartialView("_Delete",model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(DriverViewModel model)
        {
            if (ModelState.IsValid&&DriverDAL.DeleteData(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }
    }
}
﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class PerjalananController : Controller
    {
        // GET: Perjalanan
        public ActionResult Index()
        {
            List<PerjalananViewModel> list = PerjalananDAL.Get();
            return View(list);
        }

        public ActionResult Detail(int id)
        {
            PerjalananDetailViewModel model = PerjalananDAL.Detail(id);
            List<PengirimanDetailViewModel> listPengirimanDetail = PengirimanDAL.Detail(id);
            model.ListPengirimanDetail = listPengirimanDetail;
            return View(model);
        }

        public ActionResult Insert()
        {
            PerjalananDetailViewModel model = new PerjalananDetailViewModel();
            model.ListDriver = DriverDAL.Get();
            model.ListKendaraan = KendaraanDAL.Get();
            model.ListPengiriman = PengirimanDAL.Get();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(PerjalananDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (PerjalananDAL.Insert(model))
                {
                    return RedirectToAction("Index", "Perjalanan");
                }
            }
            return View(model);
        }

        public ActionResult DataDriver(int id)
        {
            DriverViewModel model = DriverDAL.Get(id);
            return PartialView("_DataDriver", model);
        }

        public ActionResult DataKendaraan(int id)
        {
            KendaraanViewModel model = KendaraanDAL.Get(id);
            return PartialView("_DataKendaraan", model);
        }

        public ActionResult DataBarang(int id)
        {
            PengirimanDetailViewModel model = PengirimanDAL.Get(id);
            List<PengirimanDetailViewModel> list = PengirimanDetailDAL.Get(id);
            BarangDetailViewModel[] barang = BarangDetailDAL.Get(list).ToArray();
            model.BarangDetail = barang;
            return PartialView("_DataBarang", model);
        }

        public ActionResult DataPengiriman(int id)
        {
            PengirimanDetailViewModel model = PengirimanDAL.Get(id);
            return PartialView("_DataPengiriman", model);
        }
    }
}
﻿using Logistik.DAL;
using Logistik.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Logistik.Web.Controllers
{
    public class JenisPengirimanController : Controller
    {
        // GET: JenisPengiriman
        public ActionResult Index()
        {
            List<JenisPengirimanViewModel> list = JenisPengirimanDAL.Get();
            return View(list);
        }

        public ActionResult Detail(int id)
        {
            JenisPengirimanViewModel model = JenisPengirimanDAL.Get(id);
            return PartialView("_Detail", model);
        }

        public ActionResult List()
        {
            List<JenisPengirimanViewModel> model = JenisPengirimanDAL.Get();
            return PartialView("_List", model);
        }

        public ActionResult Insert()
        {
            return PartialView("_Insert");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(JenisPengirimanViewModel model)
        {
            if (ModelState.IsValid && JenisPengirimanDAL.Insert(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Insert");
        }

        public ActionResult Edit(int id)
        {
            JenisPengirimanViewModel model = JenisPengirimanDAL.Get(id);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(JenisPengirimanViewModel model)
        {
            if (ModelState.IsValid && JenisPengirimanDAL.Edit(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Edit");
        }

        public ActionResult Delete(int id)
        {
            JenisPengirimanViewModel model = JenisPengirimanDAL.Get(id);
            return PartialView("_Delete", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(JenisPengirimanViewModel model)
        {
            if (ModelState.IsValid && JenisPengirimanDAL.Delete(model))
            {
                var result = new { message = "success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_Delete");
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Logistik.Web.Startup))]
namespace Logistik.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
